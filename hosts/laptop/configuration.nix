{ config, pkgs, lib, ... }:

{
  boot = {
    tmpOnTmpfs = true;
    cleanTmpDir = true;
    consoleLogLevel = 0;
    kernelParams = [
      "quiet"
      "udev.log_level=3"
      "acpi_backlight=vendor"
    ];

    kernelModules = [ "vfat" "btrfs" "amdgpu" ];
    kernel.sysctl = { "vm.swappiness" = 20; };
    kernelPackages = pkgs.linuxPackages_latest;

    initrd.verbose = false;
    initrd.kernelModules = [ "vfat" "btrfs" "amdgpu" ];
    initrd.supportedFilesystems = [ "btrfs" ];

    enableContainers = false;
    supportedFilesystems = [ "btrfs" ];

    loader = {
      timeout = 3;
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        editor = false;
        graceful = false;
        consoleMode = "keep";
      };
    };
  };

  systemd.network.enable = true;
  systemd.network.networks = {
    wlan = {
      name = "wl*";
      DHCP = "yes";
      dns = [ "223.5.5.5" "2400:3200::1" ];
      networkConfig = {
        MulticastDNS = "yes";
        IPv6PrivacyExtensions = "yes";
      };
    };
    ethernet = {
      name = "en*";
      DHCP = "yes";
      dns = [ "223.5.5.5" "2400:3200::1" ];
      networkConfig = {
        MulticastDNS = "yes";
        IPv6PrivacyExtensions = "yes";
      };
    };
  };

  services.resolved.enable = true;

  networking = {
    hostId = "10241024";
    hostName = "nixos";
    useDHCP = false;
    firewall.enable = false;
    wireless = {
      enable = true;
      # networks."ChinaNet-Rhnu".psk = "";
    };
    # nameservers = [ "223.5.5.5" "223.6.6.6" "2400:32000::1" ];
    # hosts = { };
  };

  time.timeZone = "Asia/Shanghai";

  i18n = {
    defaultLocale = "en_US.UTF-8";

    extraLocaleSettings = {
      LC_COLLATE = "C";
      LC_MESSAGES = "C";
    };
  };

  console = {
    font = "";
    keyMap = "us";
  };

  users = {
    users = {
      kebo = {
        isNormalUser = true;
        description = "kebo";
        shell = pkgs.bash;
        home = "/home/kebo";
        extraGroups = [ "wheel" "audio" "video" "input" ];
      };
    };
  };

  nixpkgs.config = {
    allowUnfree = true;
    allowBroken = true;
  };

  nix = {
    settings = {
      auto-optimise-store = true;
      trusted-users = [ "root" "kebo" ];
      substituters = lib.mkForce [
        "https://mirrors.tuna.tsinghua.edu.cn/nix-channels/store"
        "https://mirrors.ustc.edu.cn/nix-channels/store"
        "https://mirror.sjtu.edu.cn/nix-channels/store"
        "https://cache.nixos.org"
      ];
    };
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      experimental-features = nix-command flakes
    '';
    gc = {
      automatic = false;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };

  environment.systemPackages = lib.attrVals [
    "fd"
    "jq"
    "bat"
    "exa"
    "zip"
    "acpi"
    "btop"
    "htop"
    "tmux"
    "tree"
    "wget"
    "file"
    "gnupg"
    "tokei"
    "unzip"
    "sqlite"
    "pfetch"
    "ripgrep"
    "pciutils"
    "usbutils"
    "nixpkgs-fmt"
    "gcc"
    "gdb"
    "gnumake"
    "python3"
  ] pkgs;

  services.openssh = {
    enable = false;
    permitRootLogin = "yes";
  };

  programs.command-not-found.enable = false;

  programs.nano.nanorc = ''
    set nowrap
    set tabsize 2
    set tabstospaces
  '';

  programs.git = {
    enable = true;
    lfs.enable = true;
    config = {
      init = {
        defaultBranch = "main";
      };
      core = {
        safecrlf = true;
        autocrlf = "input";
        ignoreCase = false;
      };
      diff = {
        algorithm = "patience";
      };
      http = {
        postBuffer = 524288000;
      };
    };
  };

  programs.bash = {
    enableLsColors = true;
    promptInit = ''
      PS1="[\u@\h \W ]\\$ "
    '';
  };

  documentation.nixos.enable = false;

  environment.etc.issue.text = lib.mkForce ''
    NixOS \r (\l)

  '';

  environment.shellAliases = {
    ".." = "cd ..";
    "..." = "cd ../..";

    md = "mkdir -p";
    la = "ls -A";
    lh = "ls -lh";
    ll = "ls -Alh";
    lth = "ls -Alth";
    ls = "ls --color=auto";

    gaa = "git add .";
    gph = "git push";
    gpl = "git pull";
    gfh = "git fetch";
    gco = "git checkout";
    gcm = "git commit -m";
    gst = "git status -sb";

    tn = "tmux new -s";
  };

  system.stateVersion = "22.05";
}
