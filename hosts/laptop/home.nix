{ config, pkgs, lib, ... }:

{
  home = {
    username = "kebo";
    homeDirectory = "/home/kebo";
    packages = lib.attrVals [
    ] pkgs;
  };

  programs = {
    bat = {
      enable = true;
      config = {
        paging = "never";
        theme = "Dracula";
      };
    };

    git = {
      enable = true;
      userName = "kebo";
      userEmail = "kebo2021@qq.com";
      aliases = {
        cm = "commit -m";
        st = "status -sb";
        new = "checkout -b";
        amend = "commit --amend -m";
        unstage = "reset HEAD^";
        uncommit = "reset --soft HEAD^";
        contributors = "shortlog --summary --numbered";
        lo = "log --oneline";
        last = "log -1 HEAD";
        logg = "log --graph --decorate --all";
        dog = "log --all --decorate --oneline --graph";
      };
      extraConfig = {
        core.editor = "nvim";
      };
    };
  };

  services = { };

  home.stateVersion = "22.05";
}
