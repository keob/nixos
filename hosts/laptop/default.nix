{ system, self, nixpkgs, home-manager, inputs }:

nixpkgs.lib.nixosSystem {
  inherit system;
  modules = [
    ./hardware.nix
    ./configuration.nix
    home-manager.nixosModules.home-manager
    {
      home-manager.useGlobalPkgs = true;
      home-manager.useUserPackages = true;
      home-manager.users.kebo = import ./home.nix;
    }
  ];
}
